//getting the request body from the input
requestBody = JSON.parse(request.content); 

billingId = requestBody.billingId;
//setting context variable to responseMessage
context.setVariable("responseMessage",JSON.stringify(requestBody)); 
//setting context variable billingId
context.setVariable("billingId",billingId); 

//
context.setVariable("amount",requestBody.payment.amount);
context.setVariable("datePosted",requestBody.payment.datePosted);
context.setVariable("bankAccountName",requestBody.payment.bankAccount.nickname);
context.setVariable("bankAccountRouting",requestBody.payment.bankAccount.routing);
context.setVariable("bankAccountNumber",requestBody.payment.bankAccount.number);